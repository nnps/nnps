from __future__ import print_function
from __future__ import division
import numpy as np
import lhapdf
import nnps.shower as shower
import nnps.reweighter as reweighter

Ntvalues = 50

# transformation into logarithmic space
def log_trans(v):
    return np.log(v)
def inv_log_trans(b):
    return np.exp(b)
def coord_distance(nbins, trans, min, max):
    return nbins / (trans(max) - trans(min))
def value(bin, nbins, trans, inv_trans, min, max):
    return inv_trans(trans(min) + (bin+0.5) / coord_distance(nbins, trans, min, max))

scale_min, scale_max = (shower.t_IR_cutoff/4, shower.t_0_mu + 2*shower.t_0_sigma)
print(scale_min, scale_max)
scale_values = np.array([value(i, Ntvalues, log_trans, inv_log_trans, scale_min, scale_max) for i in xrange(Ntvalues)])
print(scale_values)

def qacc_values(reweighter):
    return np.array([[scale, reweighter.q_acc_fac(scale)] for scale in scale_values])
