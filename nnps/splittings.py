from __future__ import print_function
from __future__ import division

from abc import ABCMeta, abstractmethod
import numpy as np

def enum(**enums):
    return type('Enum', (), enums)
Parton = enum(g="g", q="q")

CF = 4/3
NC = 3
CA = NC
TR = 1/2
nf = 5


class Splitting:
    __metaclass__ = ABCMeta

    def __init__(self, pdf, epsilon):
        self.pdf = pdf
        self.underestimated_z_min = epsilon
        self.overestimated_z_max = 1 - epsilon
        self.overestimated_z_integral = (
                self.overestimated_z_distribution(self.overestimated_z_max)
                - self.overestimated_z_distribution(self.underestimated_z_min)
                )

    def __str__(self):
        s = self.mother() + " ->"
        for parton in self.daughters():
            s += " " + parton
        return s

    def acceptance_prob(self, scale, t, z):
        return self.kernel(scale, t, z) / self.overestimated_kernel(t, z)

    def kernel(self, scale, t, z):
        return self.pdf.alphasQ2(scale) / (2*np.pi) * 1/t * self.z_kernel(z)

    @abstractmethod
    def z_kernel(self, z): pass

    @abstractmethod
    def overestimated_kernel(self, t, z): pass

    @abstractmethod
    def overestimated_z_distribution(self, z): pass

    @abstractmethod
    def mother(self): pass

    @abstractmethod
    def daughters(self): pass


class QQGSplitting(Splitting):

    def z_kernel(self, z):
        return CF * (1 + z**2) / (1 - z)

    def overestimated_kernel(self, t, z):
        return CF/3 / t / (1 - z)

    def overestimated_z_distribution(self, z):
        return -CF/3 * np.log(1 - z)

    def mother(self):
        return Parton.q

    def daughters(self):
        return [Parton.q, Parton.g]



class GGGSplitting(Splitting):

    def z_kernel(self, z):
        return CA * (z**4 + 1 + (1 - z)**4) / (z * (1 - z))

    def overestimated_kernel(self, t, z):
        return CA/3 / t / (z * (1 - z))

    def overestimated_z_distribution(self, z):
        return CA/3 * (np.log(z) - np.log(1-z))

    def mother(self):
        return Parton.g

    def daughters(self):
        return [Parton.g]*2


class GQQSplitting(Splitting):

    def z_kernel(self, z):
        # the factor two accounts for symmetry between qqbar and qbarq
        return TR*nf * (z**2 + (1-z)**2)

    def overestimated_kernel(self, t, z):
        return TR/6*nf / t

    def overestimated_z_distribution(self, z):
        return TR/6*nf * z

    def mother(self):
        return Parton.g

    def daughters(self):
        return [Parton.q]*2
