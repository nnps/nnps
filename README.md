# Neural net parton shower

Aim: Use a neural network to predict parton-shower reweighting factors for
a given observable bin.

Showcase: The curves in the image are the ratios for a given variation in the
parton-shower splitting kernels. As variations, AlphaS(mZ) (the dashed
curves) and scale factors have been used (the solid curves).
For each curve, the reweighting factor has been calculated on-the-fly
for events with a leading jet pT of at least 30 GeV.
This reweighting factor uses the algorithm that's proven to be exact for
high statistics.
We also train neural networks to predict each of those factors, given its curve
as the input. The predicted factor and its corresponding curves are
omitted from the training. The ratio of this predicted factor over the true
result ("nn/otf") is given in the plots on the right.
We show results for a shower with competitive g->gg, g->qqbar and q->qg
splittings. Only the reweighting to the lowest scale (blue solid curve)
is off by about 0.15 %. This is the most difficult case for the neural nets,
as it has to extrapolate. This is also true for the lowest curve (reweighting
to highest scale, yellow solid curve), but the distance to the next curve is
smaller.

![Showcase](showcase.png)

- [ ] Repeat the exercise for initial-state showers, i.e. where the kernel
  includes a PDF ratio f_new(x/z)/f_old(x), maybe use some PDF
  values/ratios as additional input (?)
- [ ] Finally, mix initial- and final-state showering, can the neural net still
  cope with that?
