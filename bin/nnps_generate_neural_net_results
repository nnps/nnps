#!/usr/bin/env python2
"""uses a fully-connected ReLU network with one hidden layer, and trains by
minimising squared Euclidean distance"""

from __future__ import print_function
from __future__ import division
import random
import argparse
import os
import numpy as np
import torch
from torch.autograd import Variable
from nnps.histo import get_histo_names, histo_from_name, all_histo_names
import nnps.util

# define/parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--histogram', default='all', choices=['all'] + all_histo_names)
parser.add_argument('--nreplicas', default=10, type=float)
parser.add_argument('--bin', type=int)
parser.add_argument('--loss-target', type=float, default=1e-4)
parser.add_argument('--max-steps', type=float, default=1e5)
parser.add_argument('--rew-fac', type=int)
parser.add_argument('--num-training-rew-facs', type=int, default=-1)
parser.add_argument('--reset-seed', action='store_true')
parser.add_argument('--mode', default='validation', choices=['final', 'validation'])
parser.add_argument('--randomly-dismiss-data-points', type=int, default=-1)
parser.add_argument('--results-suffix', default=None)
args = parser.parse_args()

# validate/process arguments
histo_names = get_histo_names(args)
if args.bin is not None and len(histo_names) != 1:
    raise Exception("--bin=<n> can only be used when "
                    "--histogram=<single_histogram> is specified, too")
bin_only = args.bin
rew_fac_only = args.rew_fac
num_rewfacs = args.num_training_rew_facs
if num_rewfacs > -1 and rew_fac_only >= num_rewfacs:
    raise Exception("--rew-fac must be smaller than --num-training-rew-facs")

if not rew_fac_only is None and args.mode != 'validation':
    raise Exception("--rew-fac can only be used when --mode=validation")

if args.randomly_dismiss_data_points != -1 and args.num_training_rew_facs != -1:
    raise Exception("--randomly-dismiss-data-points can not be used when "
                    "--num-training-rew-facs is specfied, too")

results_dir = nnps.util.results_dir(args.results_suffix)

rewtags = np.loadtxt(results_dir + 'rewtags.dat', dtype=(str))

# find indizes for asmz and assf variations (will needed for taking random
# samples)
asmz_rewtag_indizes = [i for i, tag in enumerate(rewtags) if not '.' in tag]
assf_rewtag_indizes = [i for i, tag in enumerate(rewtags) if '.' in tag]

# find indizes of most extreme variations
asmz_outer_rewtag_indizes = sorted([[float(tag), i] for i, tag in enumerate(rewtags) if not '.' in tag])
asmz_outer_rewtag_indizes = [asmz_outer_rewtag_indizes[0][1]] + [asmz_outer_rewtag_indizes[-1][1]]
assf_outer_rewtag_indizes = sorted([[float(tag), i] for i, tag in enumerate(rewtags) if '.' in tag])
assf_outer_rewtag_indizes = [assf_outer_rewtag_indizes[0][1]] + [assf_outer_rewtag_indizes[-1][1]]

# get input data, which determines the number of variations N and the input
# dimension D_in
N = 0
x_all = np.array([])
while num_rewfacs == -1 or N < num_rewfacs:
    try:
        print("load qacc #{}".format(N), "...")
        qacc = np.load(results_dir + 'qacc_{}.npy'.format(N))
        x_all = np.append(x_all, qacc[:,1])
    except IOError:
        print("No file for rew index {}. Stop.".format(N))
        break
    N += 1
N_replicas = int(args.nreplicas)
D_in = len(x_all)//N
x_all = x_all.reshape((N, D_in))

# determine remaining variables of the NN architecture; n_hidden_layer is
# hidden dimension, D_out is output dimension.
n_hidden_layer = D_in//4
D_out = 1

print("NN input/output dimensions: {}, {}".format(D_in, D_out))
with open(results_dir + 'nn_arch.dat', 'w') as arch_file:
    arch_file.write('D_in\t{}\n'.format(D_in))
    arch_file.write('D_out\t{}\n'.format(D_out))
    arch_file.write('n_hidden_layer\t{}'.format(n_hidden_layer))

# output training data: rows are the observable bins, columns the variations
for histo_name in histo_names:
    print("processing histo", histo_name)
    histo_results_dir = results_dir + histo_name

    try:
        y_all_over_bins = np.load(histo_results_dir + '/rewfacavg.npy')[:,:,0].T
        y_all_over_bins_err = np.load(histo_results_dir + '/rewfacavg.npy')[:,:,1].T
    except IndexError:
        y_all_over_bins = np.load(histo_results_dir + '/rewfacavg.npy').T
        y_all_over_bins_err = np.zeros(y_all_over_bins.shape)

    while y_all_over_bins.shape[1] > N:
        y_all_over_bins = np.delete(y_all_over_bins, y_all_over_bins.shape[1]-1, 1)
        y_all_over_bins_err = np.delete(y_all_over_bins_err, y_all_over_bins_err.shape[1]-1, 1)

    # loop over observable bins
    bins = range(y_all_over_bins.shape[0]) if bin_only is None else [bin_only]
    for histo_bin in bins:
        print("processing bin", histo_bin)
        bin_results_dir = histo_results_dir + '/bin-{}'.format(histo_bin)
	if args.randomly_dismiss_data_points >= 0:
	    bin_results_dir += "-{}".format(args.randomly_dismiss_data_points)
	    bin_results_dir += "_random_omissions"

        try:
            os.makedirs(bin_results_dir)
        except OSError:
            pass

        y_all = y_all_over_bins[histo_bin].reshape((N, D_out))
        y_all_err = y_all_over_bins_err[histo_bin].reshape((N, D_out))

        # validation: cycle over data samples, remove nth, train with the
        # others, predict nth, and do all of that N_replicas times (each time,
        # generate a MC replica of the data sample treating its uncertainty as
        # a Gaussian) to have a handle on the accuracy

        # final: cycle over data samples, train with entire input, do that
        # N_replicas times and save each of the trained models

        if args.mode == 'validation':
            n_range = range(N) if rew_fac_only is None else [rew_fac_only]
        else:
            n_range = [-1]
        for n in n_range:
            if args.mode == 'validation':
                print("processing rew fac", n)
            untrained_results = []
            m = 0
            while m < N_replicas:
                m += 1
                if args.mode == 'validation':
                    print("predict rew fac #{} (pass #{}) ...".format(n, m))
                else:
                    print("train model (pass #{}) ...".format(m))
                if args.reset_seed:
                    print("reset seed to 0")
                    torch.manual_seed(0)

                if args.mode == 'validation':
                    x = np.delete(x_all, n, 0)
                    y = np.delete(y_all, n, 0)
                else:
                    x = x_all
                    y = y_all

                # vary data according to its uncertainty (treating the error as Gaussian)
                y_replica = np.zeros((y.shape[0], y.shape[1]))
                for i, (value, error) in enumerate(zip(y, y_all_err)):
                    y_replica[i] = random.gauss(value, error)

                # take random sample of training data points
                if args.randomly_dismiss_data_points >= 0:
                    asmz_inner_rewtag_indizes = [i for i in asmz_rewtag_indizes if i not in asmz_outer_rewtag_indizes]
                    assf_inner_rewtag_indizes = [i for i in assf_rewtag_indizes if i not in assf_outer_rewtag_indizes]
                    asmz_k = len(asmz_inner_rewtag_indizes) - args.randomly_dismiss_data_points
                    assf_k = len(assf_inner_rewtag_indizes) - args.randomly_dismiss_data_points
                    asmz_training_indizes = random.sample(asmz_inner_rewtag_indizes, asmz_k)
                    asmz_training_indizes += asmz_outer_rewtag_indizes
                    assf_training_indizes = random.sample(assf_inner_rewtag_indizes, assf_k)
                    assf_training_indizes += assf_outer_rewtag_indizes
                    training_indizes = asmz_training_indizes + assf_training_indizes
                    x = x.take(training_indizes, axis=0)
                    y_replica = y_replica.take(training_indizes, axis=0)
                    print("use training indizes: {}".format(training_indizes))

                # Create Tensors to hold inputs and outputs, and wrap them in Variables.
                xtensor = torch.FloatTensor(x)
                ytensor = torch.FloatTensor(y_replica)
                xvar = Variable(xtensor)
                yvar = Variable(ytensor, requires_grad=False)

                # Use the nn package to define our model as a sequence of layers. nn.Sequential
                # is a Module which contains other Modules, and applies them in sequence to
                # produce its output. Each Linear Module computes output from input using a
                # linear function, and holds internal Variables for its weight and bias.
                model = torch.nn.Sequential(
                    torch.nn.Linear(D_in, n_hidden_layer),
                    torch.nn.ReLU(),
                    torch.nn.Linear(n_hidden_layer, D_out),
                )

                # The nn package also contains definitions of popular loss functions; in this
                # case we will use Mean Squared Error (MSE) as our loss function.
                loss_fn = torch.nn.MSELoss(size_average=False)

                learning_rate = 1e-4
                optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
                last_loss = 1e20
                last_keyframe_losses = []
                cancelled = False
                t = 0
                while(last_loss > args.loss_target and t < args.max_steps):
                    # Forward pass: compute predicted y by passing x to the model. Module objects
                    # override the __call__ operator so you can call them like functions. When
                    # doing so you pass a Variable of input data to the Module and it produces
                    # a Variable of output data.
                    y_pred = model(xvar)

                    # Compute and print loss. We pass Variables containing the predicted and true
                    # values of y, and the loss function returns a Variable containing the
                    # loss.
                    loss = loss_fn(y_pred, yvar)
                    current_loss = loss.item()

                    is_at_keyframe = t % 5000 == 0
                    if is_at_keyframe:
                        last_keyframe_losses.append(current_loss)
                        if len(last_keyframe_losses) == 6:
                            del last_keyframe_losses[0]
                            did_find_improvement = False
                            for i in xrange(4):
                                if last_keyframe_losses[i] * 0.999 > last_keyframe_losses[i+1]:
                                    did_find_improvement = True
                                    break
                            if did_find_improvement:
                                last_keyframe_losses = []
                            else:
                                last_loss = current_loss
                                print(t, last_loss, "(cancel optimisation after no improvement over 5 keyframes)")
                                cancelled = True
                                break
                    last_loss = current_loss
                    if is_at_keyframe:
                        last_keyframe_loss = last_loss
                        print(t, last_keyframe_loss)
                    t = t + 1

                    # Zero the gradients before running the backward pass.
                    model.zero_grad()

                    # Backward pass: compute gradient of the loss with respect to all the learnable
                    # parameters of the model. Internally, the parameters of each Module are stored
                    # in Variables with requires_grad=True, so this call will compute gradients for
                    # all learnable parameters in the model.
                    loss.backward()

                    # Update the weights using gradient descent. Each parameter is a Variable, so
                    # we can access its data and gradients like we did before.
                    #for param in model.parameters():
                    #    param.data -= learning_rate * param.grad.data

                    # Calling the step function on an Optimizer makes an update to its
                    # parameters
                    optimizer.step()

                print(t, last_loss)

                if cancelled and last_loss > 1e-2:
                    print("optimisation was stuck and the loss is larger than 1e-2!")
                    print("=> assume we fell into a local minimum, retry training pass ...")
                    m -= 1
                    continue

                x_all_var = Variable(torch.FloatTensor(x_all))
                results = np.array([y_all, model(x_all_var).data.numpy()]).T[0]
                for i, row in enumerate(results):
                    if i == n:
                        print(">", end="")
                    else:
                        print(" ", end="")
                    print((row[0] - row[1])/row[0], end="")
                    if i == n:
                        print("\tabs:", row[1], end="")
                    print()

                if args.mode == 'validation':
                    untrained_results.append(results[n][1])
                    print(untrained_results)
                else:
                    torch.save(model.state_dict(), bin_results_dir + '/nn_model-{}'.format(m))

            if args.mode == 'validation':
                untrained_results = np.array(untrained_results)
                combined_untrained_results = np.zeros(2)
                combined_untrained_results[0] = untrained_results.mean()
                combined_untrained_results[1] = untrained_results.std()
                np.save(bin_results_dir + '/untrained_rew_fac-{}'.format(n), combined_untrained_results)
