"""A shower that only does gluon emissions from a single quark line"""

from __future__ import print_function
from __future__ import division
import random
import numpy as np
import lhapdf
import splittings

uses_pt_like_alpha_s_scale = True  # cf. (5.57) in Ellis Stirling Webber

pdf = lhapdf.mkPDF("NNPDF31_nnlo_as_0118", 0)

# the mean and the standard deviation of the starting scale t_0
t_0_mu = 1e4
t_0_sigma = 1.0*t_0_mu

t_IR_cutoff = 1

# the overestimated limits on z
epsilon = t_IR_cutoff/(t_0_mu+3*t_0_sigma)

available_splittings = {
        splittings.Parton.q: [splittings.QQGSplitting(pdf, epsilon)],
        splittings.Parton.g: [splittings.GGGSplitting(pdf, epsilon), splittings.GQQSplitting(pdf, epsilon)]
        }

def generate_history():
    partons = {splittings.Parton.q: 1, splittings.Parton.g: 0}
    history = ShowerHistory()
    last_t = random.gauss(t_0_mu, t_0_sigma)
    while last_t > t_IR_cutoff:
        next_splitting = None
        next_t = 0
        for parton, multiplicity in partons.iteritems():
            for _ in range(multiplicity):
                for splitting in available_splittings[parton]:
                    t_candidate = last_t
                    while next_t == 0:
                        t_candidate = random_t_from_overestimated_distribution(t_candidate, splitting)
                        if t_candidate < t_IR_cutoff:
                            break
                        z_candidate = random_z_from_overestimated_distribution(splitting)
                        acc_prob, scale = acceptance_prob_and_scale(splitting, t_candidate, z_candidate)
                        if acc_prob > random.random():
                            history.append_accepted_emission(
                                    TrialEmission(scale, t_candidate, z_candidate))
                            if t_candidate > next_t:
                                next_t = t_candidate
                                next_splitting = splitting
                        elif acc_prob != 0.0:
                            history.append_rejected_emission(
                                    RejectedTrialEmission(scale, t_candidate, z_candidate, acc_prob))
        last_t = next_t
        if next_splitting is not None:
            partons[next_splitting.mother()] -= 1
            for daughter in next_splitting.daughters():
                partons[daughter] += 1
    return history

def random_t_from_overestimated_distribution(last_t, splitting):
    R = random.random()
    t = inverse_z_integrated_overestimated_kernel(
            np.log(R) + z_integrated_overestimated_kernel(last_t, splitting), splitting)
    return t

def random_z_from_overestimated_distribution(splitting):
    R = random.random()
    zhigh = 1 - splitting.underestimated_z_min
    zlow = 1 - splitting.overestimated_z_max
    return 1 - zhigh * (zlow/zhigh)**R

def z_min(t):
    return t_IR_cutoff/t

def z_max(t):
    return 1 - t_IR_cutoff/t

def inverse_z_integrated_overestimated_kernel(x, splitting):
    return np.exp(x / splitting.overestimated_z_integral)

def z_integrated_overestimated_kernel(t, splitting):
    return np.log(t) * splitting.overestimated_z_integral

def acceptance_prob_and_scale(splitting, t_candidate, z_candidate):
    scale = 0
    acc_prob = 0
    if z_min(t_candidate) < z_candidate and  z_candidate < z_max(t_candidate):
        if uses_pt_like_alpha_s_scale:
            scale = z_candidate * (1 - z_candidate) * t_candidate
        else:
            scale = t_candidate
        acc_prob = splitting.acceptance_prob(scale, t_candidate, z_candidate)
    if acc_prob < 0:
        raise Exception("acc_prob < 0")
    if acc_prob > 1:
        raise Exception("acc_prob > 1")
    return acc_prob, scale


class ShowerHistory:
    """A record for all accepted and rejected trial emissions of a single
    shower run"""

    def __init__(self):
        self.accepted_emissions = []
        self.rejected_emissions = []

    def append_accepted_emission(self, emission):
        self.accepted_emissions.append(emission)

    def append_rejected_emission(self, emission):
        self.rejected_emissions.append(emission)

class TrialEmission:
    def __init__(self, scale, t, z):
        self.scale = scale
        self.t = t
        self.z = z

class RejectedTrialEmission(TrialEmission):
    def __init__(self, scale, t, z, acceptance_prob):
        TrialEmission.__init__(self, scale, t, z)
        self.acceptance_prob = acceptance_prob

if __name__ == "__main__":
    histo_range = (t_IR_cutoff, t_0_mu + 3*t_0_sigma)
    print("# splitting histos")
    random.seed(1)
    for event in range(10):
        history = generate_history()
        accepted_t_values = [em.t for em in history.accepted_emissions]
        rejected_t_values = [em.t for em in history.rejected_emissions]
        accepted_t_values_histo = np.histogram(accepted_t_values,
                                               range=histo_range)
        rejected_t_values_histo = np.histogram(rejected_t_values,
                                               range=histo_range)
        print("event {} acc:".format(event),
                accepted_t_values_histo[0],
                "rej:",
                rejected_t_values_histo[0])
