from setuptools import setup

setup(
   name='nnps',
   version='0.1',
   description='A neural-net powered parton-shower reweighter',
   author='Enrico Bothmann, Luigi Del Debbio',
   author_email='enrico.bothmann@ed.ac.uk',
   url="https://gitlab.com/ebothmann/nnps",
   packages=['nnps'],
   install_requires=['lhapdf', 'matplotlib', 'numpy', 'torch'],
   scripts=[
            'bin/nnps_generate_training_data',
            'bin/nnps_generate_neural_net_results',
            'bin/nnps_make_plots',
            'bin/nnps_make_plots3d',
            'bin/nnps_make_reweighting_plots',
           ]
)
