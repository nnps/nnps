def results_dir(suffix=None):
    results_dir = 'results'
    if suffix is not None:
        results_dir += '-{}'.format(suffix)
    results_dir += '/'
    return results_dir

