install-dev:
	pip2 install -e . && rehash

clean: clean-results clean-readme clean-doc

results:
	nnps_generate_training_data && nnps_generate_neural_net_results && nnps_make_plots

clean-results:
	rm -rf results

README.html: README.md
	pandoc -s README.md -o README.html

clean-readme:
	rm -rf README.html

doc:
	for f in doc/*.md ; do pandoc --mathjax -s "$$f" -o "$${f:r}.html" ; done

clean-doc:
	rm -rf doc/*.html

.PHONY: install-dev results clean clean-results clean-readme doc
