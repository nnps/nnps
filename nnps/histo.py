"""Observable histogram definitions (and related argument parsing)"""

from __future__ import print_function
from __future__ import division
import numpy as np
import nnps.shower as shower

all_histo_names = ['ptlead', 'ptlead_gt_30', 'nemissions', 'thrust']

def get_histo_names(args):
    histo_args = args.histogram.split(',')
    if not 'all' in histo_args:
        return histo_args
    else:
        names = list(all_histo_names)
        names.remove('thrust')
        return names

def histo_from_name(name):
    if name == 'ptlead':
        edges = np.logspace(np.log10(shower.t_IR_cutoff),
                            np.log10(shower.t_0_mu + 3*shower.t_0_sigma),
                            num=9)
    elif name == 'ptlead_gt_30':
        edges = np.array([0, 1])
    elif name == 'nemissions':
        edges = np.linspace(0, 8, num=9)
    else:
        raise Exception("Unknown histo " + name)
    bins = np.zeros(len(edges) - 1)
    return {'edges': edges, 'bins': bins}

def nbins_from_name(name):
    if name == 'ptlead':
        return 8
    elif name == 'ptlead_gt_30':
        return 1
    elif name == 'nemissions':
        return 8
    elif name == 'thrust':
        return 42

def fill_histo(history, histo_name, histo):
    histo_bin = None
    if histo_name == 'ptlead':
        if not len(history.accepted_emissions) == 0:
            t_lead = history.accepted_emissions[0].t
            histo_bin = -1
            for edge in histo['edges']:
                if edge >= t_lead:
                    break
                histo_bin += 1
    elif histo_name == 'ptlead_gt_30':
        if not len(history.accepted_emissions) == 0 and history.accepted_emissions[0].t > 30**2:
            histo_bin = 0
    elif histo_name == 'nemissions':
        nemissions = len(history.accepted_emissions)
        histo_bin = -1
        for edge in histo['edges']:
            if edge >= nemissions:
                break
            histo_bin += 1
    else:
        raise Exception("Unknown histo " + histo_name)
    if histo_bin < 0 or histo_bin >= len(histo['bins']):
        histo_bin = None  # ignore under-/overflow
    if histo_bin is None:
        return None
    histo['bins'][histo_bin] += 1
    return histo_bin
