#!/usr/bin/env python2

from __future__ import print_function
from __future__ import division
import sys
import lhapdf

class Reweighter:

    def __init__(self, scalefactor=1.0, pdf=None):
        self.scalefactor = scalefactor
        self.cv_pdf = lhapdf.mkPDF("NNPDF31_nnlo_as_0118", 0)
        self.pdf = pdf if pdf is not None else self.cv_pdf

    def rew_fac(self, history):
        fac = 1.0
        accepted_emission_vals = [em.scale for em in history.accepted_emissions]
        rejected_emission_vals = [(em.scale, em.acceptance_prob) for em in history.rejected_emissions]
        for scale in accepted_emission_vals:
            fac *= self.q_acc_fac(scale)
        for scale, acceptance_prob in rejected_emission_vals:
            fac *= self.q_rej_fac(scale, acceptance_prob)
        return fac

    def q_acc_fac(self, scale):
        return self.pdf.alphasQ2(self.scalefactor*scale) / self.cv_pdf.alphasQ2(scale)

    def q_rej_fac(self, scale, acceptance_prob):
        return 1 + (1 - self.q_acc_fac(scale)) * acceptance_prob / (1 - acceptance_prob)

    def __str__(self):
        s  = " SF={}".format(self.scalefactor)
        s += " AlphaS(mZ)={}".format(self.pdf.info().get_entry("AlphaS_MZ"))
        return s


if __name__ == "__main__":
    import shower
    import random
    import math
    print("# splitting histos")
    random.seed(1)
    reweighter = Reweighter("muR", 4.00)
    factor_avg = 0.0
    factor_avg_std = 0.0
    Nevents = 100000
    for event in range(Nevents):
        history = shower.generate_history()
        factor = reweighter.rew_fac(history)
        factor_avg += factor
        factor_avg_std += factor**2
        if event % (Nevents // 100) == 0:
            print("\r{} %".format((event * 100) // Nevents), end=', ')
            running_factor_avg = factor_avg / event
            running_factor_avg_std = math.sqrt(factor_avg_std/event - running_factor_avg**2)
            print("average factor: {} +- {}".format(running_factor_avg, running_factor_avg_std), end='')
            sys.stdout.flush()
    print()
